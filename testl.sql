-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8889
-- Generation Time: Jul 23, 2019 at 06:04 PM
-- Server version: 5.6.38
-- PHP Version: 7.1.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `testl`
--

-- --------------------------------------------------------

--
-- Table structure for table `bookings`
--

CREATE TABLE `bookings` (
  `id` int(10) UNSIGNED NOT NULL,
  `room_id` int(10) UNSIGNED NOT NULL,
  `date_start` datetime NOT NULL,
  `date_end` datetime NOT NULL,
  `customer_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `bookings`
--

INSERT INTO `bookings` (`id`, `room_id`, `date_start`, `date_end`, `customer_id`, `created_at`, `updated_at`) VALUES
(1, 13, '2020-02-01 20:47:16', '2020-02-07 20:47:16', 4, '2019-07-04 17:47:16', '2019-07-04 17:47:16'),
(2, 17, '2019-10-30 20:47:16', '2019-11-03 20:47:16', 2, '2019-07-04 17:47:16', '2019-07-04 17:47:16'),
(3, 5, '2020-01-29 20:47:16', '2020-02-03 20:47:16', 5, '2019-07-04 17:47:16', '2019-07-04 17:47:16'),
(4, 11, '2019-11-22 20:47:16', '2019-11-28 20:47:16', 3, '2019-07-04 17:47:16', '2019-07-04 17:47:16'),
(5, 9, '2019-11-24 20:47:16', '2019-11-29 20:47:16', 2, '2019-07-04 17:47:16', '2019-07-04 17:47:16'),
(6, 1, '2019-09-15 20:47:16', '2019-09-22 20:47:16', 5, '2019-07-04 17:47:16', '2019-07-04 17:47:16'),
(7, 11, '2020-02-28 20:47:16', '2020-03-07 20:47:16', 5, '2019-07-04 17:47:16', '2019-07-04 17:47:16'),
(8, 14, '2019-11-20 20:47:16', '2019-11-26 20:47:16', 6, '2019-07-04 17:47:16', '2019-07-04 17:47:16'),
(9, 4, '2019-10-14 20:47:16', '2019-10-18 20:47:16', 5, '2019-07-04 17:47:16', '2019-07-04 17:47:16'),
(10, 16, '2020-02-26 20:47:16', '2020-03-02 20:47:16', 5, '2019-07-04 17:47:16', '2019-07-04 17:47:16'),
(11, 13, '2020-01-21 20:47:16', '2020-01-26 20:47:16', 3, '2019-07-04 17:47:16', '2019-07-04 17:47:16'),
(12, 19, '2019-09-02 20:47:16', '2019-09-10 20:47:16', 5, '2019-07-04 17:47:16', '2019-07-04 17:47:16'),
(13, 7, '2020-02-22 20:47:16', '2020-02-27 20:47:16', 3, '2019-07-04 17:47:16', '2019-07-04 17:47:16'),
(14, 9, '2019-10-23 20:47:16', '2019-10-25 20:47:16', 2, '2019-07-04 17:47:16', '2019-07-04 17:47:16'),
(15, 8, '2019-11-16 20:47:16', '2019-11-22 20:47:16', 2, '2019-07-04 17:47:16', '2019-07-04 17:47:16'),
(16, 7, '2019-09-21 20:47:16', '2019-09-24 20:47:16', 5, '2019-07-04 17:47:16', '2019-07-04 17:47:16'),
(17, 18, '2020-02-20 20:47:16', '2020-02-27 20:47:16', 2, '2019-07-04 17:47:16', '2019-07-04 17:47:16'),
(18, 9, '2020-03-03 20:47:16', '2020-03-05 20:47:16', 2, '2019-07-04 17:47:16', '2019-07-04 17:47:16'),
(19, 19, '2020-01-05 20:47:16', '2020-01-12 20:47:16', 4, '2019-07-04 17:47:16', '2019-07-04 17:47:16'),
(20, 15, '2019-07-29 20:47:16', '2019-08-01 20:47:16', 3, '2019-07-04 17:47:16', '2019-07-04 17:47:16');

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` int(10) UNSIGNED NOT NULL,
  `first_name` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(1024) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fax` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `first_name`, `last_name`, `address`, `city`, `country`, `phone`, `fax`, `created_at`, `updated_at`) VALUES
(2, 'Jaleel', 'Harber', '66528 Mohr Cliffs\nEast Zulaton, ID 79709', 'Leolafort', 'Netherlands', '+5123889245286', '+9929610878615', '2019-07-04 17:47:16', '2019-07-04 17:47:16'),
(3, 'Claire', 'Baumbach', '2421 Eldora Tunnel\nWisokybury, AK 54978-6519', 'Marilouland', 'Namibia', '+1152767874235', '+1099317258966', '2019-07-04 17:47:16', '2019-07-04 17:47:16'),
(4, 'Dorthy', 'Cronin', '6845 Marielle Estate Apt. 741\nWest Parkerhaven, AK 33421-7853', 'Starkberg', 'Ghana', '+7952614741358', '+9766830141989', '2019-07-04 17:47:16', '2019-07-04 17:47:16'),
(5, 'Narciso', 'Auer', '8261 Ernest Plains\nSchambergerview, CO 17848', 'South Jackeline', 'Cocos (Keeling) Islands', '+9363022381957', '+3883887722290', '2019-07-04 17:47:16', '2019-07-04 17:47:16'),
(6, 'Macy', 'Corkery', '635 D\'Amore Underpass\nEast Ernest, NJ 87607-2556', 'Omerborough', 'Serbia', '+6124818666813', '+1323228243845', '2019-07-04 17:47:16', '2019-07-04 17:47:16');

-- --------------------------------------------------------

--
-- Table structure for table `dynamic_prices`
--

CREATE TABLE `dynamic_prices` (
  `id` int(10) UNSIGNED NOT NULL,
  `price` decimal(8,2) NOT NULL,
  `week_day` enum('Mon','Tue','Wed','Thu','Fri','Sat','Sun') COLLATE utf8mb4_unicode_ci NOT NULL,
  `room_type_id` int(10) UNSIGNED NOT NULL,
  `room_capacity_id` int(10) UNSIGNED NOT NULL,
  `date_from` datetime NOT NULL,
  `date_to` datetime NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `hotels`
--

CREATE TABLE `hotels` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(1024) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(1024) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `state` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `zip` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL,
  `img` varchar(2048) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `hotels`
--

INSERT INTO `hotels` (`id`, `name`, `address`, `city`, `state`, `country`, `zip`, `phone`, `email`, `img`, `created_at`, `updated_at`) VALUES
(1, 'Bernhard PLC', '24280 Stacy Key Suite 378\nRoobmouth, MD 34043', 'Port Wadeland', 'Michigan', 'Suriname', '99879', '+1103927854374', 'sboyle@example.org', 'https://lorempixel.com/640/480/city/?82184', '2019-07-04 17:47:16', '2019-07-04 17:47:16');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(117, '2014_10_12_000000_create_users_table', 1),
(118, '2014_10_12_100000_create_password_resets_table', 1),
(119, '2019_06_29_203010_create_hotels_table', 1),
(120, '2019_06_29_203015_create_room_types_table', 1),
(121, '2019_06_29_203029_create_room_capacities_table', 1),
(122, '2019_06_29_203031_create_rooms_table', 1),
(123, '2019_06_29_203051_create_prices_table', 1),
(124, '2019_06_29_203105_create_dynamic_prices_table', 1),
(125, '2019_06_29_203120_create_customers_table', 1),
(126, '2019_06_29_203134_create_bookings_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `prices`
--

CREATE TABLE `prices` (
  `id` int(10) UNSIGNED NOT NULL,
  `price` decimal(8,2) NOT NULL,
  `week_day` enum('Mon','Tue','Wed','Thu','Fri','Sat','Sun') COLLATE utf8mb4_unicode_ci NOT NULL,
  `room_type_id` int(10) UNSIGNED NOT NULL,
  `room_capacity_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `rooms`
--

CREATE TABLE `rooms` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `hotel_id` int(10) UNSIGNED NOT NULL,
  `room_type_id` int(10) UNSIGNED NOT NULL,
  `room_capacity_id` int(10) UNSIGNED NOT NULL,
  `img` varchar(2048) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `rooms`
--

INSERT INTO `rooms` (`id`, `name`, `hotel_id`, `room_type_id`, `room_capacity_id`, `img`, `created_at`, `updated_at`) VALUES
(1, 'A11', 1, 2, 2, 'https://lorempixel.com/640/480/city/?42775', '2019-07-04 17:47:16', '2019-07-11 17:11:29'),
(4, 'A41', 1, 3, 3, 'https://lorempixel.com/640/480/city/?54216', '2019-07-04 17:47:16', '2019-07-11 17:11:40'),
(5, 'A5', 1, 1, 1, 'https://lorempixel.com/640/480/city/?72011', '2019-07-04 17:47:16', '2019-07-04 17:47:16'),
(7, 'A7', 1, 2, 2, 'https://lorempixel.com/640/480/city/?13349', '2019-07-04 17:47:16', '2019-07-04 17:47:16'),
(8, 'B8', 1, 3, 2, 'https://lorempixel.com/640/480/city/?73925', '2019-07-04 17:47:16', '2019-07-04 17:47:16'),
(9, 'B9', 1, 2, 4, 'https://lorempixel.com/640/480/city/?20795', '2019-07-04 17:47:16', '2019-07-04 17:47:16'),
(10, 'B10', 1, 1, 1, 'https://lorempixel.com/640/480/city/?44621', '2019-07-04 17:47:16', '2019-07-04 17:47:16'),
(11, 'B11', 1, 1, 3, 'https://lorempixel.com/640/480/city/?60974', '2019-07-04 17:47:16', '2019-07-04 17:47:16'),
(12, 'B12', 1, 1, 1, 'https://lorempixel.com/640/480/city/?55679', '2019-07-04 17:47:16', '2019-07-04 17:47:16'),
(13, 'B13', 1, 2, 2, 'https://lorempixel.com/640/480/city/?21910', '2019-07-04 17:47:16', '2019-07-04 17:47:16'),
(14, 'B14', 1, 3, 3, 'https://lorempixel.com/640/480/city/?40227', '2019-07-04 17:47:16', '2019-07-04 17:47:16'),
(15, 'B15', 1, 1, 1, 'https://lorempixel.com/640/480/city/?78135', '2019-07-04 17:47:16', '2019-07-04 17:47:16'),
(16, 'C16', 1, 2, 3, 'https://lorempixel.com/640/480/city/?57891', '2019-07-04 17:47:16', '2019-07-04 17:47:16'),
(17, 'C17', 1, 2, 1, 'https://lorempixel.com/640/480/city/?13522', '2019-07-04 17:47:16', '2019-07-04 17:47:16'),
(18, 'C18', 1, 2, 3, 'https://lorempixel.com/640/480/city/?35774', '2019-07-04 17:47:16', '2019-07-04 17:47:16'),
(19, 'C19', 1, 1, 1, 'https://lorempixel.com/640/480/city/?16181', '2019-07-04 17:47:16', '2019-07-04 17:47:16'),
(20, 'C20', 1, 2, 3, 'https://lorempixel.com/640/480/city/?94185', '2019-07-04 17:47:16', '2019-07-04 17:47:16'),
(21, 'room1', 1, 2, 2, 'https://s-ec.bstatic.com/images/hotel/max1024x768/731/73118462.jpg', '2019-07-10 16:43:07', '2019-07-10 16:43:07'),
(22, 'cd2', 1, 2, 1, 'http://www.bestwesternplusmeridian.com/Content/images/Queen-Room.jpg', '2019-07-10 16:47:02', '2019-07-10 16:47:02'),
(23, 'cd3', 1, 3, 4, 'https://www.corinthia.com/application/files/9114/7582/3493/business-twin.jpg', '2019-07-10 16:48:12', '2019-07-10 16:48:12'),
(24, 'A1', 1, 3, 1, 'https://lorempixel.com/640/480/city/?42775', '2019-07-10 18:11:07', '2019-07-10 18:11:07'),
(25, 'A6', 1, 2, 2, 'https://lorempixel.com/640/480/city/?30861', '2019-07-10 18:12:02', '2019-07-10 18:12:02'),
(26, 'A2', 1, 1, 4, 'https://lorempixel.com/640/480/city/?10744', '2019-07-10 18:12:55', '2019-07-10 18:12:55'),
(27, 'A1', 1, 3, 1, 'https://lorempixel.com/640/480/city/?42775', '2019-07-10 18:13:18', '2019-07-10 18:13:18'),
(28, 'aaq11', 1, 3, 4, 'https://www.hotel-badischerhof-badenbaden.com/radisson/baden-baden/rooms/image-thumb__6590__radisson_mosaic-zoom/bad21-business-class02.low-res.gif', '2019-07-11 17:19:23', '2019-07-11 17:19:23');

-- --------------------------------------------------------

--
-- Table structure for table `room_capacities`
--

CREATE TABLE `room_capacities` (
  `id` int(10) UNSIGNED NOT NULL,
  `capacity` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `room_capacities`
--

INSERT INTO `room_capacities` (`id`, `capacity`, `created_at`, `updated_at`) VALUES
(1, 'Single', '2019-07-04 17:47:16', '2019-07-04 17:47:16'),
(2, 'Double', '2019-07-04 17:47:16', '2019-07-04 17:47:16'),
(3, 'Quad', '2019-07-04 17:47:16', '2019-07-04 17:47:16'),
(4, 'Twin', '2019-07-04 17:47:16', '2019-07-04 17:47:16');

-- --------------------------------------------------------

--
-- Table structure for table `room_types`
--

CREATE TABLE `room_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `type` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `room_types`
--

INSERT INTO `room_types` (`id`, `type`, `created_at`, `updated_at`) VALUES
(1, 'Standard', '2019-07-04 17:47:16', '2019-07-04 17:47:16'),
(2, 'Deluxe', '2019-07-04 17:47:16', '2019-07-04 17:47:16'),
(3, 'Superior', '2019-07-04 17:47:16', '2019-07-04 17:47:16');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role` enum('admin','customer') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'customer',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `role`, `created_at`, `updated_at`) VALUES
(1, 'Ethel Harvey IV', 'aa@aa.com', NULL, '$2y$10$1OJAbhZLgm8cDtglgEz.t.B8/69Zco0KE83DzVII7G0BnSe7DHS/.', NULL, 'admin', '2019-07-04 17:47:16', '2019-07-04 17:47:16'),
(2, 'Genoveva Veum III', 'walter.newton@example.com', NULL, '1ypx6359zzpxfl02n4y4', NULL, 'customer', '2019-07-04 17:47:16', '2019-07-04 17:47:16'),
(3, 'Jarred Kessler', 'felipa46@example.org', NULL, 'itt886ftlkcbc54o0p34', NULL, 'customer', '2019-07-04 17:47:16', '2019-07-04 17:47:16'),
(4, 'Maxime Champlin', 'rosendo.tromp@example.net', NULL, '778fu8utojxy077qjy56', NULL, 'customer', '2019-07-04 17:47:16', '2019-07-04 17:47:16'),
(5, 'Luther Hoeger', 'jettie.ritchie@example.com', NULL, '3lj1puv1krckcpatde49', NULL, 'customer', '2019-07-04 17:47:16', '2019-07-04 17:47:16'),
(6, 'Dr. Yoshiko Langworth', 'vivienne65@example.org', NULL, '308b6c7aq0c4cm59v63x', NULL, 'customer', '2019-07-04 17:47:16', '2019-07-04 17:47:16'),
(7, '', 'asd@asd.com', NULL, '$2y$10$dQwtRvBMmqrtTOvoCTVKxuOpXy0fRiSfr2A42spXrHRMMpcjVzNg2', NULL, 'customer', '2019-07-10 10:02:54', '2019-07-10 10:02:54'),
(8, '', 'asda@asd.com', NULL, '$2y$10$xLdRxMnAa5raop8rEjV25eu8DVKb1qsoZAF27x5yKNYZXl5/0sley', NULL, 'customer', '2019-07-10 10:03:09', '2019-07-10 10:03:09');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bookings`
--
ALTER TABLE `bookings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `bookings_room_id_foreign` (`room_id`),
  ADD KEY `bookings_customer_id_foreign` (`customer_id`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD UNIQUE KEY `customers_id_unique` (`id`);

--
-- Indexes for table `dynamic_prices`
--
ALTER TABLE `dynamic_prices`
  ADD PRIMARY KEY (`id`),
  ADD KEY `dynamic_prices_room_type_id_foreign` (`room_type_id`),
  ADD KEY `dynamic_prices_room_capacity_id_foreign` (`room_capacity_id`);

--
-- Indexes for table `hotels`
--
ALTER TABLE `hotels`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `prices`
--
ALTER TABLE `prices`
  ADD PRIMARY KEY (`id`),
  ADD KEY `prices_room_type_id_foreign` (`room_type_id`),
  ADD KEY `prices_room_capacity_id_foreign` (`room_capacity_id`);

--
-- Indexes for table `rooms`
--
ALTER TABLE `rooms`
  ADD PRIMARY KEY (`id`),
  ADD KEY `rooms_hotel_id_foreign` (`hotel_id`),
  ADD KEY `rooms_room_type_id_foreign` (`room_type_id`),
  ADD KEY `rooms_room_capacity_id_foreign` (`room_capacity_id`);

--
-- Indexes for table `room_capacities`
--
ALTER TABLE `room_capacities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `room_types`
--
ALTER TABLE `room_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bookings`
--
ALTER TABLE `bookings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `dynamic_prices`
--
ALTER TABLE `dynamic_prices`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `hotels`
--
ALTER TABLE `hotels`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=127;

--
-- AUTO_INCREMENT for table `prices`
--
ALTER TABLE `prices`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rooms`
--
ALTER TABLE `rooms`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `room_capacities`
--
ALTER TABLE `room_capacities`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `room_types`
--
ALTER TABLE `room_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `bookings`
--
ALTER TABLE `bookings`
  ADD CONSTRAINT `bookings_customer_id_foreign` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`),
  ADD CONSTRAINT `bookings_room_id_foreign` FOREIGN KEY (`room_id`) REFERENCES `rooms` (`id`);

--
-- Constraints for table `customers`
--
ALTER TABLE `customers`
  ADD CONSTRAINT `customers_id_foreign` FOREIGN KEY (`id`) REFERENCES `users` (`id`);

--
-- Constraints for table `dynamic_prices`
--
ALTER TABLE `dynamic_prices`
  ADD CONSTRAINT `dynamic_prices_room_capacity_id_foreign` FOREIGN KEY (`room_capacity_id`) REFERENCES `room_capacities` (`id`),
  ADD CONSTRAINT `dynamic_prices_room_type_id_foreign` FOREIGN KEY (`room_type_id`) REFERENCES `room_types` (`id`);

--
-- Constraints for table `prices`
--
ALTER TABLE `prices`
  ADD CONSTRAINT `prices_room_capacity_id_foreign` FOREIGN KEY (`room_capacity_id`) REFERENCES `room_capacities` (`id`),
  ADD CONSTRAINT `prices_room_type_id_foreign` FOREIGN KEY (`room_type_id`) REFERENCES `room_types` (`id`);

--
-- Constraints for table `rooms`
--
ALTER TABLE `rooms`
  ADD CONSTRAINT `rooms_hotel_id_foreign` FOREIGN KEY (`hotel_id`) REFERENCES `hotels` (`id`),
  ADD CONSTRAINT `rooms_room_capacity_id_foreign` FOREIGN KEY (`room_capacity_id`) REFERENCES `room_capacities` (`id`),
  ADD CONSTRAINT `rooms_room_type_id_foreign` FOREIGN KEY (`room_type_id`) REFERENCES `room_types` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
