<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoomType extends Model
{
    protected $fillable = ['type'];

    public function rooms()
    {
        return $this->hasMany('App\Room', 'room_type_id');
    }

    public function prices()
    {
        return $this->hasMany('App\Price', 'room_type_id');
    }

    public function dynamicPrices()
    {
        return $this->hasMany('App\DynamicPrice', 'room_type_id');
    }
}
