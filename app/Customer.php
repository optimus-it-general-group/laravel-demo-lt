<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    public function bookings()
    {
        return $this->hasMany('App\Booking', 'customer_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'id', 'id');
    }
}
