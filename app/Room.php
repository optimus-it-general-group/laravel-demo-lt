<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    protected $fillable = ['name', 'hotel_id', 'room_type_id', 'room_capacity_id', 'img'];

    public function roomCapacity()
    {
        return $this->belongsTo('App\RoomCapacity', 'room_capacity_id');
    }

    public function roomType()
    {
        return $this->belongsTo('App\RoomType', 'room_type_id');
    }

    public function hotel()
    {
        return $this->belongsTo('App\Hotel', 'hotel_id');
    }

    public function bookings()
    {
        return $this->hasMany('App\Booking', 'room_id');
    }
}
