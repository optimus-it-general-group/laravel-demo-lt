<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DynamicPrice extends Model
{
    public function roomCapacity()
    {
        return $this->belongsTo('App\RoomCapacity', 'room_capacity_id');
    }

    public function roomType()
    {
        return $this->belongsTo('App\RoomType', 'room_type_id');
    }
}
