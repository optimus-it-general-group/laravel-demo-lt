<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Hotel;
use Illuminate\Support\Facades\Validator;

class HotelController extends Controller
{
    public function show($id)
    {
        return Hotel::find($id);
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|min:3|max:1024',
            'address'  => 'required|min:3|max:1024',
            'city'  => 'required|min:3|max:64',
            'state'  => 'required|min:3|max:64',
            'country'  => 'required|min:3|max:64',
            'zip'  => 'required|min:1|max:5',
            'phone'  => 'required|min:10|max:15',
            'email'  => 'required|min:5|max:128|email',
            'img'  => 'required|min:5|max:2048',
        ]);
        if ($validator->fails())
        {
            return response()->json([
                'status' => 'validation_error',
                'errors' => $validator->errors()
            ], 422);
        }

        try
        {
            $hotel = Hotel::find($id);
            $request->offsetUnset('id');
            $hotel->update($request->all());
        }
        catch(\Exception $e)
        {
            return response()->json(['status' => $e->getMessage()], 500);
        }

        return response()->json(['status' => 'success'], 200);
    }
}
