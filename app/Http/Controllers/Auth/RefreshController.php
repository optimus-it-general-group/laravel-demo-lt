<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Http\Controllers\Auth\AuthController;

class RefreshController extends Controller
{
    /**
     * Refresh JWT token
     */
    public function refresh()
    {
        $authController = new AuthController();
        if ($token = $authController->guard()->refresh()) {
            return response()
                ->json(['status' => 'successs'], 200)
                ->header('Authorization', $token);
        }
        return response()->json(['error' => 'refresh_token_error'], 401);
    }
}
