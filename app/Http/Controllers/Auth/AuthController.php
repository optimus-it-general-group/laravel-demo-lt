<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Auth;

class AuthController extends Controller
{
    /**
     * Return auth guard
     */
    public function guard()
    {
        return Auth::guard();
    }
}
