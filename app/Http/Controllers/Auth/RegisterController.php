<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Customer;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;

use Illuminate\Auth\Events\Registered;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    /*
    protected function validator(array $data)
    {
        return Validator::make($data, [
            // 'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:1', 'max:255', 'confirmed'],
        ]);
    }
*/

    /**
     * Register a new user
     */
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email|min:1|unique:users',
            'password'  => 'required|min:1|confirmed',
        ]);
        if ($validator->fails())
        {
            return response()->json([
                'status' => 'validation_error',
                'errors' => $validator->errors()
            ], 422);
        }

        try{
            // Firstly add user record (as system user - admin or manager or customer)
            $user = new User();
            $user->email = $request->email;
            $user->password = bcrypt($request->password);
            $user->role = 'customer';
            $user->save();

            // Add customer record (as client)
            $customer = new Customer();
            $customer->id = $user->id;
            $customer->email = $user->email;
            $customer->save();
         }
         catch(\Exception $e){
            // do task when error
            return response()->json(['status' => $e->getMessage()], 500);
         }

        return response()->json(['status' => 'success'], 200);
    }

    /**
     * Return auth guard
     */
    private function guard()
    {
        return Auth::guard();
    }
    
}
