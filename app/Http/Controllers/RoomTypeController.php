<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\RoomType;
use Illuminate\Support\Facades\Validator as Validator;

class RoomTypeController extends Controller
{
    public function index()
    {
        return RoomType::all();
    }

    public function store(Request $request)
    {
        return $this->saveRoomType($request);
    }

    public function update(Request $request, $id) 
    {
        return $this->saveRoomType($request, $id);
    }

    public function show($id)
    {
        return RoomType::find($id);
    }

    protected function saveRoomType(Request $request, $id = false)
    {
        $validator = Validator::make($request->all(), [
            'type' => 'required|min:1|max:32',
        ]);
        if ($validator->fails())
        {
            return response()->json([
                'status' => 'validation_error',
                'errors' => $validator->errors()
            ], 422);
        }

        // save room type
        try
        {
            if(!empty($id))
            {
                $roomType = RoomType::find($id);
            }
            else 
            {
                $roomType = new RoomType();
            }

            $input = $request->all();
            $roomType->fill($input)->save();
        }
        catch(\Exception $e)
        {
            return response()->json(['status' => $e->getMessage()], 500);
        }

        return response()->json(['status' => 'success'], 200);
    }

    public function delete(Request $request, $id) 
    {
        try
        {
            $room = RoomType::findOrFail($id);
            $room->delete();
        }
        catch(\Exception $e)
        {
            // send responce when error
            return response()->json(['status' => $e->getMessage()], 500);
        }

        return response()->json(['status' => 'success'], 200);
    }
}
