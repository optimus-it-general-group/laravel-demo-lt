<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Room;
use Illuminate\Support\Facades\Validator as Validator;

class RoomController extends Controller
{
    public function index()
    {
        return Room::with('roomType')->with('roomCapacity')->get();
    }

    public function store(Request $request)
    {
        return $this->saveRoom($request);
    }

    public function update(Request $request, $id) 
    {
        return $this->saveRoom($request, $id);
    }

    public function show($id) 
    {
        return Room::find($id);
    }

    protected function saveRoom(Request $request, $id = false)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|min:1|max:32',
            'room_type_id'  => 'required|not_in:0',
            'room_capacity_id'  => 'required|not_in:0',
            'img'  => 'required|min:5|max:2048',
        ]);
        if ($validator->fails())
        {
            return response()->json([
                'status' => 'validation_error',
                'errors' => $validator->errors()
            ], 422);
        }

        if(!empty($id))
        {
            $room = Room::find($id);
        }
        else 
        {
            $room = new Room();
        }

        // save room
        try
        {
            $input = $request->all();
            $input['hotel_id'] = 1; // hardcode because we have only one hotel
            unset($input['id']);
            $room->fill($input)->save();
        }
        catch(\Exception $e)
        {
            // send responce when error
            return response()->json(['status' => $e->getMessage()], 500);
        }

        return response()->json(['status' => 'success'], 200);
    }

    public function delete(Request $request, $id) 
    {
        try
        {
            $room = Room::findOrFail($id);
            $room->delete();
        }
        catch(\Exception $e)
        {
            // send responce when error
            return response()->json(['status' => $e->getMessage()], 500);
        }

        return response()->json(['status' => 'success'], 200);
    }
}
