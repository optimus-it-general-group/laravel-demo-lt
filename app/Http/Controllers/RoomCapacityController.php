<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\RoomCapacity;

class RoomCapacityController extends Controller
{
    public function index()
    {
        return RoomCapacity::all();
    }
}
