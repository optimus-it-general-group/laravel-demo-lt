<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use Illuminate\Foundation\Auth\User;

class UserController extends Controller
{
        /**
     * Get authenticated user
     */
    public function user(Request $request)
    {
        $userId = Auth::user()->id;
        $user = User::whereId($userId)->first([
            'name',
            'email',
            'role',
            'created_at',
            'updated_at'
        ]);
        return response()->json([
            'status' => 'success',
            'data' => $user->toArray()
        ]);
    }
}
