<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoomCapacity extends Model
{
    public function rooms()
    {
        return $this->hasMany('App\Room', 'room_capacity_id');
    }

    public function prices()
    {
        return $this->hasMany('App\Price', 'room_capacity_id');
    }

    public function dynamicPrices()
    {
        return $this->hasMany('App\DynamicPrice', 'room_capacity_id');
    }
}
