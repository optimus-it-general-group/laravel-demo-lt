<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

// Route::get('articles', 'ArticleController@index');

// Public routes
Route::post('register', 'Auth\RegisterController@register');
Route::post('login', 'Auth\LoginController@login');
Route::get('refresh', 'Auth\RefreshController@refresh');

// Routes for authentificated users
Route::middleware('jwt.auth')->group(function()
{
    Route::post('logout', 'Auth\LogoutController@logout');
    
    Route::get('rooms', 'RoomController@index');
    Route::post('rooms', 'RoomController@store');
    Route::get('rooms/{id}', 'RoomController@show');
    Route::put('rooms/{id}', 'RoomController@update');
    Route::delete('rooms/{id}', 'RoomController@delete');

    Route::get('hotels/{id}', 'HotelController@show');
    Route::put('hotels/{id}', 'HotelController@update');

    Route::get('room-types', 'RoomTypeController@index');
    Route::post('room-types', 'RoomTypeController@store');
    Route::get('room-types/{id}', 'RoomTypeController@show');
    Route::put('room-types/{id}', 'RoomTypeController@update');
    Route::delete('room-types/{id}', 'RoomTypeController@delete');

    Route::get('room-capacities', 'RoomCapacityController@index');

    Route::get('admin-dashboard', 'AdminDashboardController@index')->middleware('isAdmin');

    Route::get('booking-dashboard', 'BookingDashboardController@index')->middleware('isCustomer'); 
});