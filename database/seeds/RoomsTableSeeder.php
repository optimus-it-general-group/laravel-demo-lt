<?php

use Illuminate\Database\Seeder;
use App\RoomCapacity;
use App\RoomType;
use App\Room;
use App\Hotel;

class RoomsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // I know that I have to create seed in separated classes, but I create them in one because of 
        // time economy

        
        // Truncate tables
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        Room::truncate();
        RoomType::truncate();
        RoomCapacity::truncate();
        Hotel::truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        $faker = \Faker\Factory::create();

        // hotels seed
        Hotel::create([
            'name' => $faker->company(),
            'address' => $faker->address(),
            'city' => $faker->city(),
            'state' => $faker->state(),
            'country' => $faker->country(),
            'zip' => substr($faker->postcode(), 0, 5),
            'phone' => $faker->e164PhoneNumber(),
            'email' => $faker->safeEmail(),
            'img' => $faker->imageUrl(640, 480, 'city'),
        ]);

        // room types seed
        RoomType::create([
            'type' => 'Standard',
        ]);
        RoomType::create([
            'type' => 'Deluxe',
        ]);
        RoomType::create([
            'type' => 'Superior',
        ]);

        // room capacities seed
        RoomCapacity::create([
            'capacity' => 'Single',
        ]);
        RoomCapacity::create([
            'capacity' => 'Double',
        ]);
        RoomCapacity::create([
            'capacity' => 'Quad',
        ]);
        RoomCapacity::create([
            'capacity' => 'Twin',
        ]);

        for ($i = 0; $i < 20; $i++) {
            $namePrefix = 'A';
            if ($i > 6 && $i < 15)
            {
                $namePrefix = 'B';
            }
            elseif ($i > 14)
            {
                $namePrefix = 'C';
            }
            Room::create([
                'name' => $namePrefix . ($i + 1),
                'hotel_id' => 1, // permanent
                'room_type_id' => rand(1, 3),
                'room_capacity_id' => rand(1, 4),
                'img' => $faker->imageUrl(640, 480, 'city'),
            ]);
        }
    }
}
