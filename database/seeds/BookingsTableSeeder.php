<?php

use Illuminate\Database\Seeder;
use App\Booking;
use App\Customer;
use App\User;

class BookingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // I know that I have to create seed in separated classes, but I create them in one because of 
        // time economy

        
        // Truncate tables
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        Booking::truncate();
        Customer::truncate();
        User::truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        $faker = \Faker\Factory::create();

        // Create admin 
        User::create([
            'name' => $faker->name(),
            'email' => 'aa@aa.com', // user with id = 1 is admin
            'password' => '$2y$10$1OJAbhZLgm8cDtglgEz.t.B8/69Zco0KE83DzVII7G0BnSe7DHS/.', // password: 123
            'role' => 'admin',
        ]);
        // customers seed
        for ($i = 0; $i < 5; $i++)
        {
            $user = User::create([
                'name' => $faker->name(),
                'email' => $faker->safeEmail(),
                'password' => $faker->bothify('********************'),
                'role' => 'customer',
            ]);
            Customer::create([
                'id' => $user->id,
                'first_name' => $faker->firstName(),
                'last_name' => $faker->lastName(),
                'address' => $faker->address(),
                'city' => $faker->city(),
                'country' => $faker->country(),
                'phone' => $faker->e164PhoneNumber(),
                'fax' => $faker->e164PhoneNumber(),
            ]);
        }

        // bookings seed
        for ($i = 0; $i < 20; $i++)
        {
            $date = new DateTime();
            $date_start = clone $date->modify('+ ' . rand(1, 255) . ' day');
            $date_end = $date->modify('+ ' . rand(2, 8) . ' day');
            Booking::create([
                'room_id' => rand(1, 20),
                'date_start' => $date_start,
                'date_end' => $date_end,
                'customer_id' => rand(2, 6),
            ]);
        }
    }
}
