<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDynamicPricesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dynamic_prices', function (Blueprint $table) {
            $table->increments('id');
            $table->decimal('price', 8, 2);
            $table->enum('week_day', ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']);
            $table->unsignedInteger('room_type_id');
            $table->unsignedInteger('room_capacity_id');
            $table->dateTime('date_from');
            $table->dateTime('date_to');
            $table->timestamps();

            $table->foreign('room_type_id')->references('id')->on('room_types');
            $table->foreign('room_capacity_id')->references('id')->on('room_capacities');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dynamic_prices');
    }
}
