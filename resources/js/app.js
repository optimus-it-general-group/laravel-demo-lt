import 'es6-promise/auto'
import axios from 'axios'
import VueAxios from 'vue-axios'
import VueAuth from '@websanova/vue-auth'
import VueRouter from 'vue-router'
import auth from './auth'
import router from './router'
import Menu from './components/Menu/Menu.vue'
import '../../node_modules/bootstrap/dist/css/bootstrap.min.css'
import Index from './components/Index/Index.vue'
import VuejsDialog from "vuejs-dialog"

// import style for dialog
import 'vuejs-dialog/dist/vuejs-dialog.min.css';

require('./bootstrap');

window.Vue = require('vue');
Vue.router = router
Vue.use(VueRouter);
Vue.use(VueAxios, axios)
Vue.use(VueAuth, auth)
Vue.use(VuejsDialog);

Vue.component('index', Index)
Vue.component('Menu', Menu)

const app = new Vue({
    el: '#app',
    router,
});
