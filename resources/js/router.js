import Home from './components/Home/Home.vue';
import Register from './components/Register/Register.vue';
import Login from './components/Login/Login.vue';
import Logout from './components/Logout/Logout.vue';

// Components for admin dashboard
import Room from './components/Room/Room.vue';
import RoomCreate from './components/Room/RoomCreate.vue';
import RoomEdit from './components/Room/RoomEdit.vue';
// import AdminDashboard from './components/AdminDashboard/AdminDashboard.vue';
import Hotel from './components/Hotel/Hotel.vue';
import RoomType from './components/RoomType/RoomType.vue';
import RoomTypeCreate from './components/RoomType/RoomTypeCreate.vue';
import RoomTypeEdit from './components/RoomType/RoomTypeEdit.vue';
import RoomCapacity from './components/RoomCapacity/RoomCapacity.vue';
import PriceList from './components/PriceList/PriceList.vue';
import BookingManager from './components/BookingManager/BookingManager.vue';
import Customer from './components/Customer/Customer.vue';

// Components for customer dashboard
// import BookingDashboard from './components/BookingDashboard/BookingDashboard.vue';
import BookingOnline from './components/Visitors/BookingOnline/BookingOnline.vue';
import RoomList from './components/Visitors/RoomList/RoomList.vue';
import Reservation from './components/Visitors/Reservation/Reservation.vue';
import Profile from './components/Visitors/Profile/Profile.vue';

import VueRouter from 'vue-router'

const routes = [
    // Any role type routes
    {
        path: '/',
        name: 'home',
        component: Home,
        meta: {
            auth: undefined
        }
    },

    // Not auth routes
    {
        path: '/register',
        name: 'register',
        component: Register,
        meta: {
            auth: false
        }
    },
    {
        path: '/login',
        name: 'login',
        component: Login,
        meta: {
            auth: false
        }
    },

    // Auth routes
    {
        path: '/logout',
        name: 'logout',
        component: Logout,
        meta: {
            auth: true
        }
    },

    // Admin routes
    // {
    //     path: '/admin-dahboard',
    //     name: 'adminDashboard',
    //     component: AdminDashboard,
    //     meta: {
    //         auth: {
    //             roles: 'admin',
    //             forbiddenRedirect: '/403',
    //         }
    //     }
    // },
    {
        path: '/hotel',
        name: 'hotel',
        component: Hotel,
        meta: {
            auth: {
                roles: 'admin',
                forbiddenRedirect: '/403',
            }
        }
    },
    {
        path: '/room',
        name: 'room',
        component: Room,
        meta: {
            auth: true
        }
    },
    {
        path: '/room/create',
        name: 'roomCreate',
        component: RoomCreate,
        meta: {
            auth: true
        }
    },
    {
        path: '/room/edit/:id',
        name: 'roomEdit',
        component: RoomEdit,
        meta: {
            auth: true
        }
    },
    {
        path: '/room/delete/:id',
        name: 'roomDelete',
        component: Room,
        meta: {
            auth: true
        }
    },
    {
        path: '/room-type',
        name: 'roomType',
        component: RoomType,
        meta: {
            auth: {
                roles: 'admin',
                forbiddenRedirect: '/403',
            }
        }
    },
    {
        path: '/room-type/create',
        name: 'roomTypeCreate',
        component: RoomTypeCreate,
        meta: {
            roles: 'admin',
            forbiddenRedirect: '/403',
        }
    },
    {
        path: '/room-type/edit/:id',
        name: 'roomTypeEdit',
        component: RoomTypeEdit,
        meta: {
            roles: 'admin',
            forbiddenRedirect: '/403',
        }
    },
    {
        path: '/room-type/delete/:id',
        name: 'roomTypeDelete',
        component: RoomType,
        meta: {
            auth: true
        }
    },
    {
        path: '/room-capacity',
        name: 'roomCapacity',
        component: RoomCapacity,
        meta: {
            auth: {
                roles: 'admin',
                forbiddenRedirect: '/403',
            }
        }
    },
    {
        path: '/price-list',
        name: 'priceList',
        component: PriceList,
        meta: {
            auth: {
                roles: 'admin',
                forbiddenRedirect: '/403',
            }
        }
    },
    {
        path: '/booking-manager',
        name: 'bookingManager',
        component: BookingManager,
        meta: {
            auth: {
                roles: 'admin',
                forbiddenRedirect: '/403',
            }
        }
    },
    {
        path: '/customer',
        name: 'customer',
        component: Customer,
        meta: {
            auth: {
                roles: 'admin',
                forbiddenRedirect: '/403',
            }
        }
    },    

    // Customer routes
    // {
    //     path: '/booking-dahboard',
    //     name: 'bookingDashboard',
    //     component: BookingDashboard,
    //     meta: {
    //         auth: {
    //             roles: 'customer',
    //             forbiddenRedirect: '/403',
    //         }
    //     }
    // },
    {
        path: '/booking',
        name: 'bookingOnline',
        component: BookingOnline,
        meta: {
            auth: {
                roles: 'customer',
                forbiddenRedirect: '/403',
            }
        }
    },
    {
        path: '/room-list',
        name: 'roomList',
        component: RoomList,
        meta: {
            auth: {
                roles: 'customer',
                forbiddenRedirect: '/403',
            }
        }
    },
    {
        path: '/reservation',
        name: 'reservation',
        component: Reservation,
        meta: {
            auth: {
                roles: 'customer',
                forbiddenRedirect: '/403',
            }
        }
    },
    {
        path: '/profile',
        name: 'profile',
        component: Profile,
        meta: {
            auth: {
                roles: 'customer',
                forbiddenRedirect: '/403',
            }
        }
    },
    
    
];

const router = new VueRouter({
    routes,
})

export default router;
